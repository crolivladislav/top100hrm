import './style.scss'
let burger = document.querySelector('.burger')
let opener = document.querySelector('.sidebar-opener')
let sidebar = document.querySelector('.sidebar')
let menu = document.querySelector('.menu')

burger.addEventListener('click', () => {
    menu.classList.toggle('hideon--mob')
    sidebar.classList.add('hider')
});

opener.addEventListener('click', () => {
    sidebar.classList.toggle('hider')
    menu.classList.add('hideon--mob')
});